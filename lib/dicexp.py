from collections import namedtuple
import random
import re

SimpleExpression = namedtuple("SimpleExpression",
                              ["operator", "number", "sides", "modifier_operator", "modifier"])


class Expression:
    terms: list()
    """
    A list of all terms in dice expression as SimpleNotations
    """

    def __init__(self, notation):
        self.terms = list()
        self.notation = notation

    @property
    def max(self):
        """
        The maximum value possible with this dice expression
        """
        return 0

    @property
    def min(self):
        """
        The minimum value possible with this dice expression
        """
        return 0

    @property
    def notation(self):
        """
        This dice expression as a complete die notation
        """
        notation = str()
        for expression in self.terms:
            if expression.operator != str():
                notation += f"{expression.operator}"
            if expression.number != 0 and expression.sides != 0:
                notation += f"{expression.number}d{expression.sides}"
            if expression.modifier != 0:
                notation += f"{expression.modifier_operator}{expression.modifier}"
        return notation

    @notation.setter
    def notation(self, value):
        if not self.is_valid_notation(value):
            raise InvalidExpression(value)
        self.terms.clear()
        regex = re.compile(r"([+-\/*])*(\d+)d(\d+)|([+-\/*])*(\d+)", re.IGNORECASE)
        for expression in regex.findall(value):
            operator = expression[0]
            number = int(expression[1]) if expression[1] != "" else 0
            sides = int(expression[2]) if expression[2] != "" else 0
            modifier_operator = expression[3]
            modifier = int(expression[4]) if expression[4] != "" else 0
            die_expression = SimpleExpression(operator, number, sides, modifier_operator, modifier)
            self.terms.append(die_expression)

    def roll(self):
        total_result = int()
        for expression in self.terms:
            # Begin with an expression result of zero
            expression_result = int()
            # Generate a random number for each die roll
            for i in range(expression.number):
                expression_result += random.randint(1, expression.sides)
            # Apply the modifier to the dice roll results
            if expression.modifier_operator == '-':
                expression_result -= expression.modifier
            elif expression.modifier_operator == '/':
                expression_result /= expression.modifier
            elif expression.modifier_operator == '*':
                expression_result *= expression.modifier
            else:
                expression_result += expression.modifier
            # Apply the expression result to the total result
            if expression.operator == '-':
                total_result -= expression_result
            elif expression.operator == '*':
                total_result *= expression_result
            elif expression.operator == '/':
                total_result -= expression_result
            else:
                total_result += expression_result
        return total_result

    @staticmethod
    def is_valid_notation(notation):
        regex = re.compile(r"(^\d+d\d+(?:[-+*\/](?:\d+|\d+d\d+))*?$)", re.IGNORECASE)
        return regex.match(notation) is not None


class InvalidExpression(Exception):
    """
    Raised when attempting to create a dice expression using an invalid notation
    """