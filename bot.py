# External dependencies
import logging
from discord.ext.commands import Bot


# Bot Definition
class AssistantDM(Bot):
    def __init__(self, config):
        super().__init__(
            case_insensitive=True,
            command_prefix=self.command_prefixes,
            description="Your Assistant Dungeon Master, for all your Discord play-by-post tabletop needs.",
        )
        self.config = config
        self.logger = logging.getLogger(config['bot']['logger_name'])

    @property
    def command_prefixes(self):
        return [
            "!assistantdm ",
            "!adm ",
        ]

    async def on_ready(self):
        self.logger.info(f"Logged in as {self.user}")