# External dependencies

# Internal dependencies
from discord.ext.commands import command
from lib.dicexp import Expression, InvalidExpression


@command(help="Roll virtual dice by using a dice expression.")
async def roll(ctx, dice_expression):
    try:
        expression = Expression(dice_expression)
        result = expression.roll()
        await ctx.send(f"{ctx.message.author} your result for `{expression.notation}` is `{result}`.")
    except InvalidExpression:
        await ctx.send(f"Sorry, but `{dice_expression}` does not represent a valid dice expression.")


@roll.error
async def roll_error(ctx, error):
    await ctx.send(f"An error occurred when attempting the command: `{type(error).__name__}`")
