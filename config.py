import json
import jsonschema


def load_config(config_filename, schema_filename):
    raw_schema = open(schema_filename)
    with open(config_filename) as raw_config:
        config = json.load(raw_config)
        schema = json.load(raw_schema)
        jsonschema.validate(config, schema)
        return config
