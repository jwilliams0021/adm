from threading import Lock

class Attribute:
    __round_down = True

    token: str
    expression: str

    @property
    def value(self):
        return 0

    @property
    def round_up(self):
        return not self.__round_down

    @round_up.setter
    def round_up(self, value: bool):
        self.round_down = not value

    @property
    def round_down(self):
        return self.__round_down

    @round_down.setter
    def round_down(self, value: bool):
        lock = Lock(self.__round_down)
        if lock.locked():
            self.__round_down = value
            lock.release()
