from playercharacters import Attribute


class Character:
    name = str()
    race = str()
    gender = str()
    age = int()
    description = str()

    attributes = list()
