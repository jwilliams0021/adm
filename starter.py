# External dependencies
import logging
import sys

# Internal dependencies
import commands
from config import load_config
from bot import AssistantDM


# Function definitions
def initialize_logging(config):
    """Initializes logging for the Discord bot from the configuration file"""
    # Configure the discord logger
    discord_logger = logging.getLogger('discord')
    discord_logger.setLevel(getattr(logging, config["log_settings"]["logger_mode"]))
    # Create logger handles to the log file and standard error stream
    logger_file = logging.FileHandler(filename=config["log_settings"]["file"],
                                      encoding=config["log_settings"]["encoding"],
                                      mode='w')
    logger_stream = logging.StreamHandler(stream=sys.stderr)
    # Configure formatting for logger handles
    logger_file.setFormatter(logging.Formatter(config["log_settings"]["format"],
                                               config["log_settings"]["time_format"]))
    logger_stream.setFormatter(logging.Formatter(config["log_settings"]["format"],
                                                 config["log_settings"]["time_format"]))
    # Attach the logger handles to the discord log
    discord_logger.addHandler(logger_file)
    discord_logger.addHandler(logger_stream)
    # Configure the bot logger
    bot_logger = logging.getLogger(config['bot']['logger_name'])
    bot_logger.setLevel(getattr(logging, config["bot"]["logger_mode"]))


def main():
    """Main method, entry point to being bot execution"""
    config = load_config('config/config.json', 'config/schema.json')
    initialize_logging(config)
    bot = AssistantDM(config)
    bot.add_command(commands.roll)
    bot.run(config['bot']['token'])


if __name__ == "__main__":
    main()
